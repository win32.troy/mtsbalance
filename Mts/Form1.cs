﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Mts
{
    public partial class Form1 : Form
    {
        HttpWebRequest http = WebRequest.Create("https://internet.mts.by/login") as HttpWebRequest;
        string[] user;
        string usr, pass;
        ContextMenu contextMenu;
        string balance, trafic;
        public Form1()
        {
            InitializeComponent();
           
            contextMenu = new ContextMenu();
            MenuItem menuItem1;
            menuItem1 = new MenuItem();
            MenuItem menuItem2;
            menuItem2 = new MenuItem();
            contextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { menuItem2 });
  
            menuItem2.Index = 0;
            menuItem2.Text = "Выход";
            menuItem2.Click += Exit;
            notifyIcon1.ContextMenu = contextMenu;       
            this.ShowInTaskbar = false;
            this.Visible = false;
            if (System.IO.File.Exists("user.usr"))
                user = System.IO.File.ReadAllLines("user.usr");
            else
                System.IO.File.Create("user.usr");
            usr = user[0];
            pass= user[1];
        }

        private void Exit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void RefreshBalance(object sender=null, EventArgs e=null)
        {
            webBrowser1.Navigate("https://internet.mts.by/");
        }

         void webBrowser1_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Console.WriteLine(e.Url);
            if (e.Url.Equals("https://internet.mts.by/login?referer=%2F"))
            {
                HtmlDocument doc = webBrowser1.Document;
                HtmlElement username = doc.GetElementById("login");
                HtmlElement password = doc.GetElementById("password");
                HtmlElement submit = doc.GetElementById("commit");
                username.SetAttribute("value", usr);
                password.SetAttribute("value", pass);
                submit.InvokeMember("click");

            }
            else if (e.Url.Equals("https://internet.mts.by/"))
            {
                string html = webBrowser1.DocumentText;
                balance= html.Substring(0,html.IndexOf("руб")+3);
                balance= balance.Substring(balance.LastIndexOf(">")+1);
                trafic = html.Substring(html.IndexOf("progress"));
                trafic = trafic.Substring(trafic.IndexOf("data-text=")+"data-text=".Length+1);
                trafic = trafic.Substring(0,trafic.IndexOf("\""));
                ShowAlert(balance, trafic);
                

            }
        }
        void ShowAlert(string balance,string trafic)
        {
            notifyIcon1.BalloonTipText = string.Format("Ваш баланс: {0} \r\nОстаток трафика: {1}",balance,trafic) ;
            notifyIcon1.BalloonTipTitle = "MTS Баланс";
            notifyIcon1.ShowBalloonTip(1);
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            RefreshBalance();
        }
    }
}
